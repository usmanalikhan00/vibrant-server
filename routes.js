var UserComponent = component('User');
var ProjectComponent = component('Project');
var AuthComponent = component('Auth');

module.exports = function(router){
    AuthComponent(router);
    UserComponent(router);
    ProjectComponent(router);
}
