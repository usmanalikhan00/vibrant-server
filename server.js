
const env = 'dev';

const express         = require('express');
const app             = express();
const bodyParser      = require('body-parser');
const path            = require('path');
const fs 				= require('fs');
const mongoose        = require('mongoose');
const _overides       = require(path.join(__dirname,'lib','cupcake.js'))();

const routes          = require(path.join(__dirname,'routes'));
const config          = require(path.join(__dirname,'config'))[env];
const dbInit          = require(path.join(__dirname,'dbConnection'))(config);

const cors = require('cors');

const devClientPath = 'http://localhost:8080';
const prodClientPath = 'http://34.216.198.135';

const devServerPath = 'http://localhost:8008';
const prodServerPath = 'http://34.216.198.135:8082';

var corsOptions = {
  origin: prodClientPath,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));

const port 			= config.port;                     // set our por
const router 			= express.Router();
// get an instance of the express Router

app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

routes(router);

// app.use('/uploads', express.static(path.join(__dirname, 'uploads')))
// app.use('/vendorcontracts', express.static(path.join(__dirname, 'vendorcontracts')))
// app.use('/purchaseorders', express.static(path.join(__dirname, 'purchaseorders')))
app.use('/api', router);

// IF ALL FAILS TURN TO THY LORD AND WISH FOR A MIRACLE, MEANWHILE OPEN THE APP BY DEFAULT \m/
// const __staticPath = path.join(__dirname,'app','dist'); // dev path; use ng build --output-path=[folderName]/
// console.log(path.join(__dirname));
// const __staticPath = path.join(__dirname, 'app');
// app.use('/' , express.static(__staticPath));
// app.use('/*', express.static(__staticPath));

app.listen(port);

const cron = require('node-cron');
const axios = require('axios');
const nodemailer = require('nodemailer');

cron.schedule('*/15 * * * *', function(){
    console.log('running a task every 15 minutes');

    axios.get(prodServerPath+"/api/allprojects")
    .then(resp => {
        
        const projects = resp.data[0].projects
        console.log("Projects", projects)

        for (let i=0; i<projects.length; i++) {
        
            let url = projects[i].url
            axios.get(url)
            .then(res => {
                console.log(url, "success")
                
            })
            .catch(err => {
                console.log("project is down", projects[i].url)
                sendMailAndSms(projects[i])
            })
        
        }
    })
    .catch(err => {
        console.log("error in all projects api")
    })

});

const sendMailAndSms = (project) => {
    console.log("Send mail and sms", project)
    for (let j=0; j<project.users.length; j++) {

        let getUserApi = prodServerPath+'/api/user?userId='+project.users[j]
        console.log("user fetch url", getUserApi )
        axios.get(getUserApi)
        .then(user => {
            console.log("user resp", user)
            const singleUser = user.data[0].user[0]
            
            console.log("SIngle user", singleUser)
            
            sendMail(project.name, project.url, singleUser.email);

            sendSms(project.name, project.url, singleUser.contact);

        })
        .catch(err => {
            console.log("user fetch error")
        })

    }

}

const sendSms = (projectName, projectUrl, userContact) => {

    const msg = 'Your server is down. \n Project Name: ' + projectName + ' \n URL: '
    + projectUrl
    const smsApi = 'http://mobismpp.vbox.mobi/?user=mobilink_9876&to='+userContact+'&message='+msg
    console.log("SMS api", smsApi)
    axios.get(smsApi)
    .then(smsResp => console.log(smsResp))
    .catch(err => console.log("sms send error"))

}

const sendMail = (projectName, projectUrl, userEmail) => {
    console.log("Sending email")
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 587,
        secure: false, // upgrade later with STARTTLS
        auth: {
          user: 'dev.khaleef@gmail.com',
          pass: 'Pakistan@123'
        }
    });

    const mailOptions = {
        from: 'dev.khaleef@gmail.com',
        to: userEmail,
        subject: projectName + ' is down',
        text: 'Your server is down. \n Project Name: ' + projectName + ' \n URL: '
         + projectUrl
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
    });

}

console.log('\n===================================');
// console.log('Angular 2 Application hosted on:  ' + port);
console.log('       RESTful API Endpoints on:  ' + port + ' /api');
console.log('===================================\n');
